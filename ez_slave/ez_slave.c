/*!
   ucecho -- uppercase conversion and bitstream encryption example for ZTEX USB-FPGA Module 2.13
   Copyright (C) 2009-2014 ZTEX GmbH.
   http://www.ztex.de

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 3 as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, see http://www.gnu.org/licenses/.
!*/

#include[ztex-conf.h]	// Loads the configuration macros, see ztex-conf.h for the available macros
#include[ztex-utils.h]	// include basic functions

// configure endpoint 2 as quad buffered 1024 byte input to pc
EP_CONFIG(2,0,BULK,IN,1024,4);

// select ZTEX USB FPGA Module 1.16 as target (required for FPGA configuration)
IDENTITY_UFM_2_13(10.17.0.0,0);

// this product string is also used for identification by the host software
#define[PRODUCT_STRING]["poopmaster 5200"]

// disenables high speed FPGA configuration via EP4
// EP_CONFIG(4,0,BULK,OUT,512,2);
// ENABLE_HS_FPGA_CONF(4);

ENABLE_FLASH;
ENABLE_FLASH_BITSTREAM;

// this is called automatically after FPGA configuration
// expect endpoints configured to upload bitstream to FPGA (highspeed with gpif)
#define[POST_FPGA_CONFIG][POST_FPGA_CONFIG
	IFCONFIG = // Interface Configuration
		bmBIT7 | // use internal ifclock for FIFOs/GPIF
	 	bmBIT6 | // ifclock 48 MHz
		bmBIT5;  // drive ifclk pins

	//IFCONFIG = bmBIT7;	        // internel 30MHz clock, drive IFCLK ouput, slave FIFO interface
    //SYNCDELAY;

// commands taken from AN63787 fifo/gpib back2back example
	FIFORESET = 0x02; // reset endpoint 2
	SYNCDELAY;
	FIFORESET = 0x00;  // clear NAKALL (not acknowledged) bits to resume normal operation
	SYNCDELAY;

	EP2FIFOCFG = 0x01; // set WORD FIFOs, no auto mode
	SYNCDELAY;

	// quad buffered so send 4 blank packets
	OUTPKTEND =0x82;   //arming the EP2 OUT quadruple times, as it's quad buffered.
	SYNCDELAY;
	OUTPKTEND =0x82;
	SYNCDELAY;
	OUTPKTEND =0x82;
	SYNCDELAY;
	OUTPKTEND =0x82;
	SYNCDELAY;

	// configure flags
	PINFLAGSAB = 0xE0;			// FLAGA - indexed, FLAGB - EP6FF (ep6 full flag)
	SYNCDELAY;
	PINFLAGSCD = 0x08;			// FLAGC - EP2EF (ep2 empty flag), FLAGD - indexed
	SYNCDELAY;

/*  interesting, but needed?
	PORTCCFG =0x00;                 // Write 0x00 to PORTCCFG to configure it as an I/O port
	OEC = 0x06;                     // Configure PC0 as input, PC1 as output and PC2 as output
	PC1 =0;                         // initialize PC1 state to "low"
	PC2=0;					      // initialze PC2 state to "low"
*/

// end AN63787 commands



// ztex ucecho commands
/*
    REVCTL = 0x0;	// reset
    SYNCDELAY;
    EP2CS &= ~bmBIT0;	// stall = 0
    SYNCDELAY;
    EP4CS &= ~bmBIT0;	// stall = 0

    SYNCDELAY;		// first two packages are waste
    EP4BCL = 0x80;	// skip package, (re)arm EP4
    SYNCDELAY;
    EP4BCL = 0x80;	// skip package, (re)arm EP4

    FIFORESET = 0x80;	// reset FIFO
    SYNCDELAY;
    FIFORESET = 0x82;
    SYNCDELAY;
    FIFORESET = 0x00;
    SYNCDELAY;

    OED = 255;
    run = 1;
*/
// end ztex commands

]




// Use ZTEX macros to setup interrupt when a setup packet is sent to device
ADD_EP0_VENDOR_COMMAND((0x80,,
	// SETUPDAT is 8 byte array containing the setup packet
	// set port A pins to the first "value" byte of the setup packet
	IOA = SETUPDAT[2];
,,
	NOP;
));;


// include the main part of the firmware kit, define the descriptors, ...
#include[ztex.h]

void main(void)
{
//    WORD i,size;

// init everything
    init_USB();

	OEA = 0xff;
	IOA = 0b00101001;


	while (1) {}
/*
    while (1) {
	if ( run && !(EP4CS & bmBIT2) ) {	// EP4 is not empty
	    size = (EP4BCH << 8) | EP4BCL;
	    if ( size>0 && size<=512 && !(EP2CS & bmBIT3)) {	// EP2 is not full
		for ( i=0; i<size; i++ ) {
		    IOD = EP4FIFOBUF[i];	// data from EP4 is converted to uppercase by the FPGA ...
		    EP2FIFOBUF[i] = IOB;	// ... and written back to EP2 buffer
		}
		EP2BCH = size >> 8;
		SYNCDELAY;
		EP2BCL = size & 255;		// arm EP2
	    }
	    SYNCDELAY;
	    EP4BCL = 0x80;			// skip package, (re)arm EP4
	}
    }
*/
}
