`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/28/2015 02:37:03 PM
// Design Name: 
// Module Name: hello_clock
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ez_slave(
    input wire FXCLK,
    input wire [7:0] PORTA,
    input wire [5:0] PORTC,
    
    output wire [9:0] LED1, // led panel 1 on debug board
    output wire [19:0] LED2, // led panel 2 and 3 on debug board
    
    input wire IFCLK, // usb interface clock
    
    // active negative by default
    output wire SLOE, // SLave Output Enable, causes FIFO to drive data pins
    output wire SLRD, // SLave ReaD, causes FIFO pointer to be incremented
    output reg SLWR, // Slace WRite, causes data to be written to FIFO
    
    input wire FLAGA, //
    input wire FLAGB, // when indexed,  full (active low)
    input wire FLAGC, // when indexed, empty (active low)
    input wire FLAGD, //
    
    output wire [1:0] FIFOADR, // Select USB Endpoint (2, 4, 6, or 8)
    output wire PKTEND, // signal packet to be sent before completely filled
    
    output wire [15:0] FD // 2 bytes of FIFO data
    
    
    );
    
    fifo_writer(
        .IFCLK(IFCLK),
        .RESET(PORTC[0]),
        .FIFOADR(FIFOADR),
        .SLWR(SLWR),
        .IFULL(FLAGB),
        .FD(FD)
    );
    
    assign LED1[0] = FLAGB;
    assign LED2[15:0] = FD;
    
endmodule

// continuosly writes generated data to ez-usb slave fifo.
// set to endpoint to automagically.
module fifo_writer(
        input wire IFCLK, // usb interface clock
        input wire RESET, // input to reset state machine.  won't do much.
        
        output wire [1:0] FIFOADR, // usb endpoint address
        output reg SLWR, // slave write (active low)
        
        input wire IFULL, // indexed full flag (active low). indexed means set for current endpoint
        
        output wire [15:0] FD // FIFO data
    );
    
    
    assign FIFADR = 2'd0; // permenently select endpoint 2
    
    
    localparam S_IDLE = 0,  // default / init state
               S_CHECK = 1, // checks is buffer is full or we have data to write
               S_WRITE = 2; // writes word to fifo
               
    reg [1:0] state = S_IDLE, 
              next_state = S_IDLE;
    
    // use count to generate data to send
    shortint count = 0,
             next_count = 1;
            
    
              
    always @(negedge IFCLK) begin
        // trigger state changes on negative edge of clock as ez-usb updates on positive edges
        state <= next_state;
        count <= next_count;
    end
    
    assign FD = count;
    
    always @(state) begin
        // set default values for outputs to prevent latches
        SLWR = 1; // not active
        next_state = S_IDLE; // shouldn't rely on this...
        next_count = count;
        
        case (state)
            S_IDLE: begin
                next_count = 0;
                next_state = S_CHECK; // never idle.  always something to write.
            end
            S_CHECK: begin
                if (IFULL == 0) // stall if fifo full
                    next_state = S_CHECK;
                else begin // otherwise write new data
                    next_count = count + 1;
                    next_state = S_WRITE;
                end;
            end
            S_WRITE: begin
                SLWR = 0; // active
                next_state = check;
            end
        endcase
    end
    
 endmodule
