# use pyusb to communicate with ezusb firmware
import usb.core
import usb.util

# find device based on default ztex vendor/product
dev = usb.core.find(idVendor=0x221a, idProduct=0x100)

if dev is None:
    raise ValueError("Device not found")

#  usb is crazy
dev.set_configuration()

msg = ""
while msg is not "quit":
    msg = raw_input("> ")
    if msg in ['quit', 'q', '']:
        break

    try:
        b = int(msg, 2)
    except ValueError:
        print("not a valid binary number")
        continue

    # send a setup packet
    # request type is vendor
    # request code is 0x80
    # parse inputed string as binary and send as the value
    dev.ctrl_transfer(0x40, 0x80, b, 0, "")
